POEMA: BAJO LA LLUVIA
AUTORA: JUANA DE IBARBOUROU

¡Cómo resbala el agua por mi espalda!
¡Cómo moja mi falda,
y pone en mis mejillas su frescura de nieve!
Llueve, llueve, llueve,
y voy, senda adelante,
con el alma ligera y la cara radiante,
sin sentir, sin soñar,
llena de la voluptuosidad de no pensar.

Un pájaro se baña
en una charca turbia. Mi presencia le extraña,
se detiene… me mira… nos sentimos amigos…
¡Los dos amamos muchos cielos, campos y trigos!
Después es el asombro
de un labriego que pasa con su azada al hombro
y la lluvia me cubre de todas las fragancias
de los setos de octubre.
Y es, sobre mi cuerpo por el agua empapado
como un maravilloso y estupendo tocado
de gotas cristalinas, de flores deshojadas
que vuelcan a mi paso las plantas asombradas.
Y siento, en la vacuidad
del cerebro sin sueño, la voluptuosidad
del placer infinito, dulce y desconocido,
de un minuto de olvido.
Llueve, llueve, llueve,
y tengo en alma y carne, como un frescor de nieve.
