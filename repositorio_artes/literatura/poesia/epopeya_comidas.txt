“Hermoso como vacuno joven es el canto de las ranas guisadas de entre perdices,
la alta manta doñiguana es más preciosa que la pierna de la señora más preciosa, lo más precioso que existe, para embarcarse en un curanto bien servido,
el camarón del Huasco es rico, chorreando vino y sentimiento,
como el choro de miel que se cosecha entre mujeres, entre cochayuyos de oceánica, entre laureles y vihuelas de Talcahuano por el jugo de limón otoñal de los siglos,
o como la olorosa empanada colchagüina, que agranda de caldo la garganta y clama, de horno, floreciendo los rodeos flor de durazno.
Y, que me dicen ustedes de un costillar de chancho con ajo, picantísimo,
asado en asador de maqui, en junio, a las riberas del peumo o la patagua o el boldo que resumen la atmósfera dramática del atardecer lluvioso de Quirihue o de Cauquenes,
o de la guañaca en caldo de ganso, completamente talquino o licantenino de parentela?,
no, la codorniz asada a la parrilla se come, lo mismo que se oye “el Martirio”, en las laderas aconcaguinas, y la lisa frita en el Maule, en el que el pejerrey salta a la paila sagrada de gozo, completamente fino de río, enriquecido en la lancha maulina, mientras las niñas Carreño, como sufriendo, le hacen empeño a “lo humano” y a “lo divino”, en la de gran antigüedad familiar vihuela.

….

Porque, si es preciso el hartarse con longaniza chillaneja antes de morirse,
en día lluvioso, acariciada con vino áspero, de Auquinco o Coihueco, en arpa, guitarra y acordeón bañándose, dando terribles saltos o carcajadas, saboreando el bramante pebre cuchareado y la papa parada,
también lo es paladear la prieta tuncana en agosto, cuando los chanchos parecen obispos, y los obispos parecen chanchos o hipopótamos, y bajar la comida con unos traguitos de guindado,
si … en Gualleco las pancutras se parecen a las señoritas del lugar: son acinturadas y tienen los ojos dormidos, pues, cosquillosas y regalo-nas, quitan la carita para dejarse besar en la boca, interminablemente.

….

Cantando y tomando, los empleados públicos del lugar atraviesan sin afeitarse
de una eternidad a otra eternidad, completamente de aguardiente atorados.
en aquellos amarillos, inmensos catres de bronce que cubren el Valle Central de la República de nubes azules y angelitos y el preceptor se toma su copa de tormento, exactamente en Pelequén, en Chimbarongo, en Bailahuen o en Curanilahue conmigo.

….

La chanfaina licantenina es guiso lacustre, mito de río y ribera, fluvial- oceánico y cordillerano, lugareño, aldeano, campesino, provinciano y como de iglesia, volcánico y dramático,
y el caldillo de congrio, de criadillas, de choros como la pancutra, son lancheros, hermanos de los valdivianos lancheros, que parece que tuviesen una gran gaviota nadando en el caldo sagrado, fundamental y elemental de los huesos chascones llenos de medulas o en el navío de papas con luche o cochayuyo desenfrenado,
más que el charquicán de la alga yodada, la cual lo contiene, pero lo deprime,
retostándolo como cabeza de tonto.

….

Será el chunchul trenzado, como cabellera de señorita, oloroso y confortable a la manera de un muslo de viuda, tierno como leche de virgen,
lo cosecharemos de vaquilla o novillo o ternera joven, soltera, la cual, si estando enamorada ríe y come ruidosamente, elegid la melancólica,
sirvámoslo con buendoso puré de papas, en mangas de camisa, por Renca o Lampa, acompañados de señoras condescendientes y vino mucho tinto, pero más de bastante y mucho,
cuando ojalá se celebre el onomástico del carnicero o el santo del paco de la comuna
y la niña de la casa os convida a que recitéis, como un cualquier maricón del “Pen Club”, por ejemplo,
cantad, cantad la canción nacional, proclamándoos por pues entonces vosotros
el Conquistador de la América del Sur, proclamándoos capitán de los corsarios americanos,
proclamándoos antiguo y valeroso vikingo en jubilación hasta el alba, cuando los pájaros del amanecer cantan la lágrima romántico-dramática de la luna hundida,
no sabemos cómo nos ponemos el sombrero, ni cómo se llamaba aquél del moscatel lagar ahogado.


